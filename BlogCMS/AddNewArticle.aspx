﻿<%@ Page Title="Add New Article" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddNewArticle.aspx.cs" Inherits="BlogCMS.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>

    <asp:SqlDataSource runat="server" id="add_new_article_select"
        ConnectionString="<%$ ConnectionStrings:blog_sql_db %>">
    </asp:SqlDataSource>

    <div class="inputrow">
        <asp:Label AssociatedControlID="new_title" runat="server">Title:</asp:Label>
        <asp:TextBox ID="new_title" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ControlToValidate="new_title" ErrorMessage="Enter a title" runat="server"></asp:RequiredFieldValidator>
    </div>

    <div class="inputrow">
        <asp:Label AssociatedControlID="new_author" runat="server">Author:</asp:Label>
        <asp:TextBox ID="new_author" runat="server"></asp:TextBox>
    </div>

    <div class="inputrow">
        <asp:Label AssociatedControlID="new_body" runat="server">Body:</asp:Label>
        <asp:TextBox ID="new_body" TextMode="MultiLine" Columns="200" Rows="10" runat="server"></asp:TextBox>
    </div>

    <div class="inputbtnrow">
        <asp:Button Text="Save Draft" runat="server" OnClick="SaveDraft" />
        <asp:Button Text="Publish Article" runat="server" OnClick="PubArticle" />
    </div>

    <div id="debug" runat="server"></div>

</asp:Content>
