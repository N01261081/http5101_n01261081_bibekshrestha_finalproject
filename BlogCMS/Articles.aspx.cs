﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BlogCMS
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private string basequery = "SELECT Id, Title Articles, Author, Published, convert(varchar, [Last Published],101) as 'Last Published' from Articles";
        private string PubString;

        private string articleid
        {
            get { return Request.QueryString["id"]; }
        }

        private string pubtoggle
        {
            get { return Request.QueryString["pubtoggle"]; }
        }

        private string delarticle
        {
            get { return Request.QueryString["del"]; }
        }

        private string dosearch
        {
            get { return Request.QueryString["s"]; }
        }

        private string querytext
        {
            get { return Request.QueryString["q"]; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (pubtoggle == "1")
            {
                PubUnPub();
            }
            else if (delarticle == "1")
            {
                string del_query = "DELETE FROM ARTICLES WHERE ID = " + articleid;
                del_article.InsertCommand = del_query;
                del_article.Insert();
                Response.Redirect("Articles.aspx");
            }
            else if (dosearch == "1")
            {
                basequery += " WHERE Title LIKE '%" + querytext + "%'";

                search_label.InnerHtml = "Search result for '" + querytext + "'";
                search_label.Style.Add("display", "block");
                articles_select.SelectCommand = basequery;
                article_list.DataSource = Articles_Manual_Bind(articles_select);

                article_list.DataBind();
            }
            else
            {
                articles_select.SelectCommand = basequery;
                article_list.DataSource = Articles_Manual_Bind(articles_select);

                article_list.DataBind();
            }
        }

        protected DataView Articles_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            mytbl.Columns.Add("Publish");
            mytbl.Columns.Add("Edit");
            mytbl.Columns.Add("Delete");

            foreach (DataRow row in mytbl.Rows)
            {
                row["Articles"] =
                    "<a href=\"Article.aspx?id="
                    + row["Id"]
                    + "\">"
                    + row["Articles"]
                    + "</a>";

                if (row["Author"].ToString() == "")
                {
                    row["Author"] = "Admin";
                }
                
                if (row["Last Published"].ToString() == "")
                {
                    row["Last Published"] = "Not Published";
                }
                else
                {
                    row["Last Published"] = row["Last Published"];
                }

                if (Convert.ToInt32(row["Published"]) == 0 )
                {
                    PubString = "Publish";
                }
                else
                {
                    PubString = "Unpublish";
                }
                row["Publish"] = "<a href=\"Articles.aspx?id=" + row["Id"] + "&pubtoggle=1\">" + PubString + "</a>";

                row["Edit"] = "<a href=\"EditArticle.aspx?id=" + row["Id"] + "\">Edit</a>";

                row["Delete"] = "<a href=\"Articles.aspx?id=" + row["Id"] + "&del=1\"" +
                            "onclick=\"if (!confirm('Are you sure?')) return false;\">Delete</a>";

            }

            mytbl.Columns.Remove("Id");
            mytbl.Columns.Remove("Published");
            myview = mytbl.DefaultView;

            return myview;
        }


        protected void PubUnPub()
        {
            string addquery = "UPDATE ARTICLES SET ";

            string article_basequery = "SELECT * FROM ARTICLES " +
                "WHERE ID = " + articleid;
            article_select.SelectCommand = article_basequery;

            DataView articleview = (DataView)article_select.Select(DataSourceSelectArguments.Empty);

            DataRowView articlerowview = articleview[0];
            int PubNum;

            if (articlerowview["Published"].ToString() == "1")
            {
                PubNum = 0;
            }
            else
            {
                PubNum = 1;
            }
            addquery += "Published = " + PubNum + ", [Last Published] = GETDATE()" +
                " WHERE ID = " + articleid;

            pub_article.InsertCommand = addquery;
            pub_article.Insert();

            Response.Redirect("Articles.aspx");
        }

        protected void SearchFunction(object sender, EventArgs e)
        {
            string search_title = search_box.Text.ToString();
            
            if (search_title == "")
            {
                Response.Redirect("Articles.aspx");
            }
            else
            {
                Response.Redirect("Articles.aspx?s=1&q=" + search_title);
            }
        }
    }
}