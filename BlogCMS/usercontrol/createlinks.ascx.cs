﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace BlogCMS.usercontrol
{
    public partial class WebUserControl1 : System.Web.UI.UserControl
    {
        private string basequery = "SELECT Id, Title Articles, Published from Articles";

        protected void Page_Load(object sender, EventArgs e)
        {
            nav_select.SelectCommand = basequery;
            //nav_list.DataSource = Nav_Manual_Bind(nav_select);
            Nav_Manual_Bind(nav_select);
            

        }

        protected void Nav_Manual_Bind(SqlDataSource src)
        {
            DataView articleview = (DataView)src.Select(DataSourceSelectArguments.Empty);
           
            foreach (DataRowView row in articleview)
            {
                if (Convert.ToInt16(row["Published"]) == 1)
                {
                    HtmlGenericControl li = new HtmlGenericControl("li");
                    nav_ul.Controls.Add(li);
                    HtmlGenericControl a_link = new HtmlGenericControl("a");
                    a_link.Attributes.Add("href", "Article.aspx?id=" + row["Id"]);
                    a_link.InnerText = row["Articles"].ToString();

                    li.Controls.Add(a_link);
                }
            }
        }
    }
}