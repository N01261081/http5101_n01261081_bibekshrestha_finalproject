﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Article.aspx.cs" Inherits="BlogCMS.WebForm3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2 id="article_title" runat="server"></h2>

    <asp:SqlDataSource runat="server"
        id="article_select"
        ConnectionString="<%$ ConnectionStrings:blog_sql_db %>">
    </asp:SqlDataSource>

    <asp:SqlDataSource runat="server"
        id="del_article"
        ConnectionString="<%$ ConnectionStrings:blog_sql_db %>">
    </asp:SqlDataSource>

    <asp:SqlDataSource runat="server"
        id="pub_article"
        ConnectionString="<%$ ConnectionStrings:blog_sql_db %>">
    </asp:SqlDataSource>

    <div id="article_page_content">
        <div id="article_body" runat="server">
            <span id="author_name" runat="server"></span>
            <span id="published_date" runat="server"></span>
            <div id="article_content" runat="server"></div>
        </div>
        <div id="article_page_actions" runat="server"></div>   
    </div>
</asp:Content>
