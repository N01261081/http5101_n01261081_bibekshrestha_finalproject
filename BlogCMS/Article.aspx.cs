﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BlogCMS
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        private string article_basequery = "SELECT Id, Title, Body, Author, Published, convert(varchar, [Last Published], 101) as 'Last Published' FROM ARTICLES ";
        private string PubString;

        private string articleid
        {
            get { return Request.QueryString["id"]; }
        }

        private string pubtoggle
        {
            get { return Request.QueryString["pubtoggle"]; }
        }

        private string delarticle
        {
            get { return Request.QueryString["del"]; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (pubtoggle == "1")
            {
                PubUnPub();
            }
            else if (delarticle == "1")
            {
                string del_query = "DELETE FROM ARTICLES WHERE ID = " + articleid;
                del_article.InsertCommand = del_query;
                del_article.Insert();
                Response.Redirect("Articles.aspx");
            }
            else
            {
                if (articleid == "" || articleid == null)
                {
                    article_title.InnerHtml = "No article found";
                    return;
                }
                else
                {
                    article_basequery += "WHERE ID = " + articleid;
                    article_select.SelectCommand = article_basequery;

                    DataView articleview = (DataView)article_select.Select(DataSourceSelectArguments.Empty);

                    if (articleview.ToTable().Rows.Count < 1)
                    {
                        article_title.InnerHtml = "No article found";
                        return;
                    }

                    DataRowView articlerowview = articleview[0];
                    article_title.InnerHtml = articlerowview["Title"].ToString();
                    Page.Title = articlerowview["Title"].ToString();

                    if (articlerowview["Author"].ToString() == "")
                    {
                        author_name.InnerHtml = "Admin";
                    }
                    else
                    {
                        author_name.InnerHtml = articlerowview["Author"].ToString();
                    }

                    if (articlerowview["Last Published"].ToString() == "")
                    {
                        published_date.InnerHtml = "Not published yet";
                    }
                    else
                    {
                        published_date.InnerHtml = articlerowview["Last Published"].ToString();
                    }

                    article_content.InnerHtml = articlerowview["Body"].ToString();

                    if (Convert.ToInt32(articlerowview["Published"]) == 0)
                    {
                        PubString = "Publish";
                    }
                    else
                    {
                        PubString = "Unpublish";
                    }

                    article_page_actions.InnerHtml =
                        "<a href=\"EditArticle.aspx?id=" + articleid + "\">Edit Article</a>" +
                        "<a href=\"Article.aspx?id=" + articleid + "&del=1\"" +
                            "onclick=\"if (!confirm('Are you sure?')) return false;\">Delete Article</a>" +
                        "<a href=\"Article.aspx?id=" + articleid + "&pubtoggle=1\">" + PubString + "</a>";
                    //"<button onclick=\"PubUnPub\" runat=\"server\">" + PubString + "</button>";
                }
            }
        }

        protected void PubUnPub()
        {
            string addquery = "UPDATE ARTICLES SET ";

            article_basequery = "SELECT * FROM ARTICLES " +
                "WHERE ID = " + articleid;
            article_select.SelectCommand = article_basequery;

            DataView articleview = (DataView)article_select.Select(DataSourceSelectArguments.Empty);
            
            DataRowView articlerowview = articleview[0];
            int PubNum;

            if (articlerowview["Published"].ToString() == "1")
            {
                PubNum = 0;
            }
            else
            {
                PubNum = 1;
            }


            addquery += "Published = " + PubNum + ",[Last Published] = GETDATE()" +
                " WHERE ID = " + articleid;
            
            pub_article.InsertCommand = addquery;
            pub_article.Insert();

            Response.Redirect("Article.aspx?id=" + articleid);
        }
    }
}