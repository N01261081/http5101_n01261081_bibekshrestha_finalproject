﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace BlogCMS
{
    public partial class WebForm2 : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void SaveDraft(object sender, EventArgs e)
        {
            string addquery = "INSERT INTO ARTICLES " +
            "(Title, Body, Author) VALUES";
            
            //Getting input values from user
            string title = new_title.Text.ToString();
            string body = new_body.Text.ToString();
            body.Replace("'", "\'");
            body.Replace('"', '\"');
            //body = Regex.Escape(body);
            string author = new_author.Text.ToString();


            addquery += "('" + title + "','" + body + "','" + author + "')";

            debug.InnerHtml = addquery;

            add_new_article_select.InsertCommand = addquery;
            add_new_article_select.Insert();
            Response.Redirect("Articles.aspx");
        }

        protected void PubArticle(object sender, EventArgs e)
        {
            string addquery = "INSERT INTO ARTICLES " +
            "(Title, Body, Published, Pubdate) VALUES";

            //Getting input values from user
            string title = new_title.Text.ToString();
            string author = new_author.Text.ToString();
            string body = new_body.Text.ToString();


            addquery += "('" + title + "','" + author + "','" + body + "', 1, GETDATE())";

            debug.InnerHtml = addquery;

            add_new_article_select.InsertCommand = addquery;
            add_new_article_select.Insert();
            Response.Redirect("Articles.aspx");
        }
    }
}