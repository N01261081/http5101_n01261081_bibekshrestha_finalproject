﻿<%@ Page Title="Update Article" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditArticle.aspx.cs" Inherits="BlogCMS.WebForm4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h3 runat="server" id="article_name">Update </h3>

    <asp:SqlDataSource runat="server" id="article_select"
        ConnectionString="<%$ ConnectionStrings:blog_sql_db %>">
    </asp:SqlDataSource>

    <asp:SqlDataSource runat="server" id="edit_article"
        ConnectionString="<%$ ConnectionStrings:blog_sql_db %>">
    </asp:SqlDataSource>

    <asp:SqlDataSource runat="server" id="pub_article"
        ConnectionString="<%$ ConnectionStrings:blog_sql_db %>">
    </asp:SqlDataSource>

    <div class="inputrow">
        <asp:Label AssociatedControlID="article_title" runat="server">Title:</asp:Label>
        <asp:TextBox ID="article_title" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ControlToValidate="article_title" ErrorMessage="Enter a title" runat="server"></asp:RequiredFieldValidator>
    </div>

    <div class="inputrow">
        <asp:Label AssociatedControlID="author_name" runat="server">Author:</asp:Label>
        <asp:TextBox ID="author_name" runat="server"></asp:TextBox>
    </div>

    <div class="inputrow">
        <asp:Label AssociatedControlID="article_content" runat="server">Body:</asp:Label>
        <asp:TextBox ID="article_content" TextMode="MultiLine" Columns="200" Rows="10" runat="server"></asp:TextBox>
    </div>

    <div class="inputbtnrow">
        <asp:Button Text="Update" runat="server" OnClick="UpdateArticle" />
        <asp:Button ID="pub_unpub_btn" runat="server" OnClick="PubUnPub" />
    </div>

    <div id="pubnum_debug" runat="server"></div>
    <div id="debug" runat="server"></div>



</asp:Content>
