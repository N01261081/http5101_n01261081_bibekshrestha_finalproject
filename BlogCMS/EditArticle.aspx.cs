﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BlogCMS
{
    public partial class WebForm4 : System.Web.UI.Page
    {
        private string article_basequery;
        private int PubNum;
        private DataRowView articlerowview;

        private int Articleid
        {
            get { return Convert.ToInt32(Request.QueryString["id"]); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            
            DataRowView articlerow = GetArticleInfo(Articleid);

            if (articlerow == null)
            {
                article_name.InnerHtml = "No article found";
                return;
            }

            if (!Page.IsPostBack)
            {
                article_name.InnerHtml += articlerow["Title"].ToString();

            }
            
            article_title.Text = articlerow["Title"].ToString();
            article_content.Text = articlerow["Body"].ToString();
            author_name.Text = articlerow["Author"].ToString();

            if (articlerow["Published"].ToString() == "1")
            {
                PubNum = 0;
                pub_unpub_btn.Text = "Unpublish";
            }
            else
            {
                pub_unpub_btn.Text = "Publish";
                PubNum = 1;
            }
        }

        protected void UpdateArticle(object sender, EventArgs e)
        {
            string addquery = "UPDATE ARTICLES SET ";

            //Getting input values from user
            string title = article_title.Text.ToString();
            string body = article_content.Text.ToString();
            string author = author_name.Text.ToString();


            addquery += "Title = '" + title + "', Author = '" + author + "', Body = '" + body +
                "' WHERE ID = " + Articleid;

            debug.InnerHtml = addquery;

            edit_article.InsertCommand = addquery;
            edit_article.Insert();
            Response.Redirect("Article.aspx?id=" + Articleid);
        }

        protected void PubUnPub(object sender, EventArgs e)
        {
            string addquery = "UPDATE ARTICLES SET ";

            //Getting input values from user
            string title = article_title.Text.ToString();
            string body = article_content.Text.ToString();
            string author = author_name.Text.ToString();

            DataRowView articlerow = GetArticleInfo(Articleid);

            if (articlerow["Published"].ToString() == "1")
            {
                PubNum = 0;
            }
            else
            {
                PubNum = 1;
            }


            addquery += "Title = '" + title + "', Author = '" + author + "', Body = '" + body +
                "',Published = " + PubNum + ",PubDate = GETDATE()" +
                " WHERE ID = " + Articleid;
            
            pub_article.InsertCommand = addquery;
            pub_article.Insert();
            Response.Redirect("Article.aspx?id=" + Articleid);
        }

        protected DataRowView GetArticleInfo(int articleid)
        {
            article_basequery = "SELECT * FROM ARTICLES " +
                "WHERE ID = " + articleid;
            article_select.SelectCommand = article_basequery;

            DataView articleview = (DataView)article_select.Select(DataSourceSelectArguments.Empty);

            if (articleview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            articlerowview = articleview[0];
            return articlerowview;
        }
    }
}