﻿<%@ Page Title="Manage Articles" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Articles.aspx.cs" Inherits="BlogCMS.WebForm1" %>
<asp:Content ID="Articles" ContentPlaceHolderID="MainContent" runat="server">

    <h2><%: Title %></h2>
    <div id="new-article-link">
        <a href="/AddNewArticle.aspx">Add New Article</a>
    </div>

    <asp:SqlDataSource runat="server" id="articles_select"
        ConnectionString="<%$ ConnectionStrings:blog_sql_db %>">
    </asp:SqlDataSource>

    <asp:SqlDataSource runat="server" id="article_select"
        ConnectionString="<%$ ConnectionStrings:blog_sql_db %>">
    </asp:SqlDataSource>

    <asp:SqlDataSource runat="server"
        id="del_article"
        ConnectionString="<%$ ConnectionStrings:blog_sql_db %>">
    </asp:SqlDataSource>

    <asp:SqlDataSource runat="server"
        id="pub_article"
        ConnectionString="<%$ ConnectionStrings:blog_sql_db %>">
    </asp:SqlDataSource>

    <div class="searchrow">
        <asp:TextBox ID="search_box" runat="server" placeholder="Search by Article Title"></asp:TextBox>
        <asp:Button Text="Search Articles" runat="server" OnClick="SearchFunction"/>
    </div>
    
    <div id="search_label" runat="server"></div>

    <asp:DataGrid id="article_list" runat="server" >
    </asp:DataGrid>


</asp:Content>
